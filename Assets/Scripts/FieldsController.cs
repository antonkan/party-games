using System.Collections.Generic;
using UnityEngine;
public class FieldsHandler {
    private List<FieldController> fields;

    public FieldsHandler(List<FieldController> fields) {
        this.fields = fields;
    }

    public int GetFieldsCount() => fields.Count;

    public Vector3 GetPositionOfField(int index) => fields[index].GetTransform().position;

    public FieldController.FieldType GetTypeOfField(int index) => fields[index].type;
}