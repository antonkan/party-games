using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    #region events
    public event Action OnCurrentPlayerPressedA;
    public event Action OnAnyPlayerPressedStart;
    #endregion
    
    // Update is called once per frame
    void  FixedUpdate()
    {
        var gamepad = Gamepad.current;
        if (gamepad != null) {
            if (gamepad.aButton.isPressed && OnCurrentPlayerPressedA != null) {
                OnCurrentPlayerPressedA();
            }
            if (gamepad.startButton.isPressed && OnAnyPlayerPressedStart != null) {
                OnAnyPlayerPressedStart();
            }
        }
    }
}
