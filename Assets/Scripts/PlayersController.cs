﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayersHandler
{
    private List<Player> players;
    private int currentPlayerIndex = -1;

    public Player currentPlayer
    {
        get
        {
            return players[currentPlayerIndex];
        }
    }

    public PlayersHandler(List<Player> players)
    {
        this.players = players;
    }

    public void NextPlayer()
    {
        currentPlayerIndex = (currentPlayerIndex + 1) % players.Count;
    }

    public void ForEachPlayerData(Action<int, PlayerData> block)
    {
        for (int i = 0; i < players.Count; i++)
        {
            block.Invoke(i, players[i].playerData);
        }
    }

    public void ForCurrentPlayerData(Action<int, PlayerData> block)
    {
        block.Invoke(currentPlayerIndex, currentPlayer.playerData);
    }

    public static PlayersHandler createByPlayersCount(int playersCount, Func<GameObject> instantiateGameObject)
    {
        var players = new List<Player>();
        for (int i = 0; i < playersCount; i++)
        {
            players.Add(Player.createByIndex(i, instantiateGameObject()));
        }
        return new PlayersHandler(players);
    }
}