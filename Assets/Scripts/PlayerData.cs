using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData
{
    public string name
    {
        get;
        private set;
    }
    public int position
    {
        get;
        private set;
    }
    public int points
    {
        get;
        private set;
    }
    public int money
    {
        get;
        private set;
    }

    public PlayerData(string name)
    {
        this.name = name;
        position = 0;
    }

    public void IncreasePosition()
    {
        position++;
    }

    public void IncreaseMoney(int addition) 
    {
        money += addition;
    }

    public void DecreaseMoney(int loss)
    {
        money = Mathf.Max(0, money - loss);
    }
}
