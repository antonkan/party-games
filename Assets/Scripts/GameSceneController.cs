using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class GameSceneController : MonoBehaviour
{
    #region prefabs
    public GameObject PlayerPrefab;
    #endregion

    #region local component dependencies
    private FieldsHandler fieldsHandler;
    #endregion

    #region event
    public event Action<int, bool> OnDiceNumberChanged;
    public event Action<string> OnNewPlayersTurn;
    public event Action OnNewPlayerControlsEnabled;
    public event Action OnTurnStarted;
    public event Action<GameObject> OnSwitchTargetPlayer;
    public event Action OnGameStarted;
    public event Action<int, PlayerData> OnUpdatePlayerHUD;
    public event Action<int> OnMoneyWon;
    public event Action<int> OnMoneyLost;
    #endregion

    #region state variables
    private PlayersHandler playersController;
    private InputController inputController;
    private int currentStepsLeft = 0;
    private GameState gameState = GameState.NOT_STARTED;
    #endregion

    #region game config
    private int playerCount = 4;
    #endregion

    // Start is called before the first frame update
    private void Start()
    {
        UnityEngine.Random.InitState(DateTime.Now.Second);
        var fields = GameObject
            .FindGameObjectWithTag("fieldsContainer")
            .GetComponentsInChildren<FieldController>()
            .ToList();
        fieldsHandler = new FieldsHandler(fields);
        initInputControls();

        playersController = PlayersHandler.createByPlayersCount(
            playerCount, 
            () => Instantiate(PlayerPrefab, Vector3.zero, Quaternion.identity)
        );
        playersController.ForEachPlayerData((index, player) => OnUpdatePlayerHUD(index, player));
    }

    private void onStartGame()
    {
        if (gameState == GameState.NOT_STARTED) {
            prepareNextTurn();
            if (OnGameStarted != null)
            {
                OnGameStarted();
            }
        }
    }

    private void startNextTurn()
    {
        if (gameState == GameState.READY)
        {
            if (OnTurnStarted != null)
            {
                OnTurnStarted();
            }
            StartCoroutine(GambleNextStepCount());
        }
    }

    private void finishTurn()
    {
        int playerPosition = playersController.currentPlayer.playerData.position;
        switch(fieldsHandler.GetTypeOfField(playerPosition)) {
            case FieldController.FieldType.PLUS: currentPlayerWonMoney(Constants.MONEY_PER_TURN); break;
            case FieldController.FieldType.MINUS: currentPlayerLostMoney(Constants.MONEY_PER_TURN); break;
        }
        prepareNextTurn();
    }

    private void prepareNextTurn()
    {
        playersController.NextPlayer();
        if (OnNewPlayersTurn != null)
        {
            OnNewPlayersTurn(playersController.currentPlayer.playerData.name);
        }
        if (OnSwitchTargetPlayer != null)
        {
            OnSwitchTargetPlayer(playersController.currentPlayer.gameObject);
        }
        StartCoroutine(TransitionToNewPlayer());
    }

    private void moveCurrentPlayerOneField()
    {
        var currentPlayer = playersController.currentPlayer;
        int currentPlayerPosition = currentPlayer.playerData.position;
        Vector3 targetPosition = fieldsHandler.GetPositionOfField(currentPlayerPosition + 1) + new Vector3(0f, 1.0f, 0f);
        currentPlayer.cachedPlayerController.SetNewTargetPosition(targetPosition);
        currentPlayer.cachedPlayerController.ReachedTarget += currentPlayerReachedTarget;
    }

    private void currentPlayerReachedTarget()
    {
        var currentPlayer = playersController.currentPlayer;
        currentPlayer.cachedPlayerController.ReachedTarget -= currentPlayerReachedTarget;
        currentPlayer.playerData.IncreasePosition();
        currentStepsLeft--;
        setDiceNumber(currentStepsLeft, false);
        if (currentStepsLeft == 0)
        {
            finishTurn();
        } else
        {
            moveCurrentPlayerOneField();
        }
    }

    private int getValidRandomStepsCount()
    {
        return UnityEngine.Random.Range(1, 6);
    }

    private void setDiceNumber(int number, bool gambling)
    {
        if (OnDiceNumberChanged != null)
        {
            OnDiceNumberChanged(number, gambling);
        }
    }

    private void currentPlayerWonMoney(int money) {
        OnMoneyWon(money);
        playersController.currentPlayer.WonMoney(money);
        playersController.ForCurrentPlayerData(OnUpdatePlayerHUD);
    }

    private void currentPlayerLostMoney(int money) {
        OnMoneyLost(money);
        playersController.currentPlayer.LostMoney(money);
        playersController.ForCurrentPlayerData(OnUpdatePlayerHUD);
    }

    private void initInputControls() {
        inputController = FindObjectOfType<InputController>();
        inputController.OnCurrentPlayerPressedA += startNextTurn;
        inputController.OnAnyPlayerPressedStart += onStartGame;
    }

    private IEnumerator TransitionToNewPlayer()
    {
        gameState = GameState.TRANSITION;
        yield return new WaitForSeconds(Constants.CAMERA_SWITCH_DURATION);
        gameState = GameState.READY;
        if (OnNewPlayerControlsEnabled != null)
        {
            OnNewPlayerControlsEnabled();
        }
    }

    private IEnumerator GambleNextStepCount()
    {
        gameState = GameState.GAMBLING;
        for (int i = 0; i < 10; i++)
        {
            setDiceNumber(getValidRandomStepsCount(), true);
            yield return new WaitForSeconds(0.2f);
        }
        currentStepsLeft = getValidRandomStepsCount();
        setDiceNumber(currentStepsLeft, false);
        gameState = GameState.MOVING;
        moveCurrentPlayerOneField();
    }

    enum GameState
    {
        NOT_STARTED, READY, TRANSITION, GAMBLING, MOVING
    }
}
