﻿using System.Collections;
using UnityEngine;

public class Player
{
    public GameObject gameObject
    {
        get;
    }
    public PlayerData playerData
    {
        get;
    }

    private PlayerController _cachedPlayerController;
    public PlayerController cachedPlayerController
    {
        get
        {
            if (_cachedPlayerController == null)
            {
                _cachedPlayerController = gameObject.GetComponent<PlayerController>();
            }
            return _cachedPlayerController;
        }
    }

    public Player(GameObject gameObject, PlayerData playerData)
    {
        this.gameObject = gameObject;
        this.playerData = playerData;
    }

    public void WonMoney(int money) {
        playerData.IncreaseMoney(money);
    }

    public void LostMoney(int money) {
        playerData.DecreaseMoney(money);
    }

    public static Player createByIndex(int index, GameObject playerObject)
    {
        playerObject.transform.position = new Vector3(-5.5f + (index * 2.0f), 1.0f, 10.0f);
        var playerData = new PlayerData("Player " + (index + 1));
        return new Player(playerObject, playerData);
    }
}