using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void NewTargetReachedHandler();

public class CameraController : MonoBehaviour
{
    #region local component dependencies
    private GameSceneController gameSceneController;
    #endregion

    #region config
    public float speed = 10.0f;
    #endregion

    #region state variables
    private Transform target;
    private bool movingToNewTarget = false;
    #endregion

    private void Start()
    {
        gameSceneController = FindObjectOfType<GameSceneController>();
        gameSceneController.OnSwitchTargetPlayer += setNewTarget;
    }

    private void LateUpdate()
    {
        if (!movingToNewTarget && target != null)
        {
            transform.position = target.position + Constants.CAMERA_DISTANCE;
        }
    }

    public void setNewTarget(GameObject newTarget)
    {
        target = newTarget.transform;
        StartCoroutine(MoveTo(target.position + Constants.CAMERA_DISTANCE));
    }

    private IEnumerator MoveTo(Vector3 toPosition)
    {
        movingToNewTarget = true;
        Vector3 startPosition = transform.position;
        float t = 0;
        while (t < 1.0f)
        {
            float step = Time.deltaTime / Constants.CAMERA_SWITCH_DURATION;
            t = Mathf.Min(t + step, 1.0f);
            transform.position = Vector3.Lerp(startPosition, toPosition, t);
            yield return new WaitForFixedUpdate(); 
        }
        movingToNewTarget = false;
    }
}
