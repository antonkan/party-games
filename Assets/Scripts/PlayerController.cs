using UnityEngine;
using System;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    #region events
    public event Action ReachedTarget;
    #endregion

    #region config
    public float speed = 3.0f;
    #endregion

    #region state variables
    private bool isMoving = false;
    #endregion

    public void SetNewTargetPosition(Vector3 target)
    {
        if (!isMoving)
        {
            StartCoroutine(MoveToTarget(target));
        }
    }

    private IEnumerator MoveToTarget(Vector3 target)
    {
        isMoving = true;
        while (Vector3.Distance(transform.position, target) > 0.001f)
        {
            float step = speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target, step);
            yield return new WaitForFixedUpdate();         // Leave the routine and return here in the next frame
        }
        transform.position = target;
        isMoving = false;
        if (ReachedTarget != null)
        {
            ReachedTarget();
        }
    }
}
