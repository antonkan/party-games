using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUDController : MonoBehaviour
{
    #region component dependencies
    public Text NameText;
    public Text PointsText;
    public Text MoneyText;
    #endregion

    public void updatePlayerData(PlayerData playerData)
    {
        NameText.text = playerData.name;
        PointsText.text = "Points: " + playerData.points;
        MoneyText.text = "Money: " + playerData.money + " €";
     }
}
