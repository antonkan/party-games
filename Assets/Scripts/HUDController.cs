using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    #region component dependencies
    public GameObject stepsLeftContainer;
    public Text stepsLeftText;
    public Text playersTurnText;
    public Text moneyLostOrWonText;
    public GameObject startGameHint;
    public GameObject startTurnButton;
    public List<GameObject> playerHuds;
    #endregion

    #region local dependencies
    private GameSceneController gameSceneController;
    #endregion

    private void Start()
    {
        gameSceneController = FindObjectOfType<GameSceneController>();
        gameSceneController.OnDiceNumberChanged += setStepsLeft;
        gameSceneController.OnNewPlayersTurn += onNewPlayersTurn;
        gameSceneController.OnNewPlayerControlsEnabled += onNewPlayersControlsEnabled;
        gameSceneController.OnTurnStarted += onTurnStarted;
        gameSceneController.OnGameStarted += hideStartButton;
        gameSceneController.OnUpdatePlayerHUD += updatePlayerData;
        gameSceneController.OnMoneyWon += showMoneyWonMessage;
        gameSceneController.OnMoneyLost += showMoneyLostMessage;
    }

    private void setStepsLeft(int stepsLeft, bool gambling)
    {
        if (stepsLeft > 0)
        {
            stepsLeftContainer.gameObject.SetActive(true);
            if (gambling)
            {
                stepsLeftText.color = Color.blue;
            } else
            {
                stepsLeftText.color = Color.green;
            }
            stepsLeftText.text = stepsLeft.ToString();
        }
        else
        {
            stepsLeftContainer.gameObject.SetActive(false);
        }
    }

    private void onNewPlayersTurn(string playerName)
    {
        playersTurnText.text = "It's " + playerName + "'s turn!";
        
    }

    private void onNewPlayersControlsEnabled()
    {
        startTurnButton.SetActive(true);
    }

    private void onTurnStarted() 
    {
        startTurnButton.SetActive(false);
    }

    private void hideStartButton()
    {
        startGameHint.gameObject.SetActive(false);
        startTurnButton.gameObject.SetActive(true);
    }

    private void updatePlayerData(int index, PlayerData playerData)
    {
        if (playerHuds.Count > index)
        {
            playerHuds[index].SetActive(true);
            playerHuds[index].GetComponent<PlayerHUDController>().updatePlayerData(playerData);
        }
    }

    private void showMoneyWonMessage(int money)
    {
        StartCoroutine(ShowMoneyWonOrLostText("+ " + money + " €", Color.green));
    }

    private void showMoneyLostMessage(int money)
    {
        StartCoroutine(ShowMoneyWonOrLostText("- " + money + " €", Color.red));
    }

    private IEnumerator ShowMoneyWonOrLostText(string text, Color color) {
        moneyLostOrWonText.gameObject.SetActive(true);
        moneyLostOrWonText.color = color;
        moneyLostOrWonText.text = text;
        yield return new WaitForSeconds(1.0f);
        moneyLostOrWonText.gameObject.SetActive(false);
    }
}
