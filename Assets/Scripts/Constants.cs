
using UnityEngine;

public class Constants
{
    public const float CAMERA_SWITCH_DURATION = 1.0f;
    public static Vector3 CAMERA_DISTANCE = new Vector3(0f, 3.33f, -6.79f);

    public const int MONEY_PER_TURN = 50;
}
