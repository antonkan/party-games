using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class FieldController : MonoBehaviour
{
    public enum FieldType { PLUS, MINUS}
    public FieldType type;
    // Start is called before the first frame update

    public Transform GetTransform() => gameObject.transform;
}
